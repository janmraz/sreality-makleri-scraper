const request = require("request")
const fs = require('fs')

function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        resolve(undefined);
      }
    });
  });
}
let result = [];

async function main() {
  var data = require('./output.json')
  for(let i = 0; i < data.length ; i++){
    var item = data[i]
    let req = await doRequest("https://www.sreality.cz/api/cs/v2/companies/"+item.id)
    if(req){
      let res = JSON.parse(req)
      console.log(res.estates_count + ' - ' + (i+1) )
      item.ads = res.estates_count
      if(res._embedded.sellers.sellers[0] != null){
        item.brokerName = res._embedded.sellers.sellers[0].name
        item.brokerEmail = res._embedded.sellers.sellers[0].email
        item.brokerPhone = res._embedded.sellers.sellers[0].phones[0] ? res._embedded.sellers.sellers[0].phones[0].number : ''
        item.brokerMobilePhone = res._embedded.sellers.sellers[0].phones[1] ? res._embedded.sellers.sellers[0].phones[1].number : ''
      }
      if(i < data.length-1){
          result.push(item)
      }else{
        fs.writeFile('main_output.json', JSON.stringify(result, null, 4), function (err) {
            console.log('details saved to main_output.json!');
        });
      }
    }
  }
}

main().then(()=>{
  console.log('lol',result.length);
});
