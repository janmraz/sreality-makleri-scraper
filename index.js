const request = require("request")
const fs = require('fs')

function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        resolve(undefined);
      }
    });
  });
}
let result = [];

async function main() {
  for(let i = 1; i < 150 ; i++){
    let req = await doRequest("https://www.sreality.cz/api/cs/v2/companies?page="+i)
    if(req){
      let res = JSON.parse(req)
      console.log(res._embedded.companies.length + ' - ' + i)
      result.concat(res._embedded.companies)
      if(res._embedded.companies.length !== 0){
          result = result.concat(res._embedded.companies)
      }else{
        i = 150
        fs.writeFile('output.json', JSON.stringify(result, null, 4), function (err) {
            console.log('details saved to output.json!');
        });
      }
    }
  }
}

main().then(()=>{
  console.log('lol',result.length);
});
